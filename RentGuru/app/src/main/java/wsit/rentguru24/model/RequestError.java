package wsit.rentguru24.model;

/**
 * Created by workspaceinfotech on 8/5/16.
 */
public class RequestError {

    private String params;
    private String msg;


    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
